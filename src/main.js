// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import Vuetify from 'vuetify'
 

// Vue.use(Vuetify, {
//   theme: {
//     primary: "#64B5E6",
//     secondary: "#1E88E5",
//     accent: "#9c27b0",
//     error: "#f44336",
//     warning: "#ffeb3b",
//     info: "#2196f3",
//     success: "#4caf50"
//   }
// })
Vue.config.productionTip = false
Vue.use(Vuetify)
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
